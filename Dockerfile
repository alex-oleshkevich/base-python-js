FROM registry.gitlab.com/alex-oleshkevich/base-python
RUN curl -sL https://deb.nodesource.com/setup_9.x -o nodesource_setup.sh && bash nodesource_setup.sh
RUN apt-get update && \
    apt-get install -y --no-install-recommends nodejs && \
    apt-get -y autoremove && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*
RUN npm install -g yarn
